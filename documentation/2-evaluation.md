# How to prepare your submission

Your work will be evaluated on a test set of questions, answers and sources.
Your program will take as input a CSV containing a list of questions and ids, and should output answers, prompt and urls sources used to generate your answers.
Since the hackathon evaluation team will run your code to determine your score, you must ensure that your work is as reproducible as possible.


## Format
In order to run your code, you must make your code available in a predefined format.

Your submission **must be self-contained in a single directory**. That means you must not depend on any exterior files except shared files, such the hackathon dataset or deep-learning models, that can be found in `$DSDIR`.

This directory must contain all the code and data needed to run your project, and must contain a `run.sh` bash script and a `README.md`, and can contain a `prepare.sh` script and a `preprocess.sh` script.

The role of each file is as follows:
- `run.sh`: (mandatory). It will be called with a path to the questions csv file and with a path to an output directory.
It should set-up your environment, such as Jean-Zay module loading and python environment sourcing, then run your program.
Your program should read each entry of the csv, then output the corresponding the corresponding answer, the prompt used to generate such answer and url corresponding to the document used to generate the answer.
- `prepare.sh`: this script should be included for reproducibility. It downloads the required dependencies and data to run your program. **It will not have access to a GPU.**
- `preprocess.sh`: this script should also be included for reproducibility.
It setups computation that usually run only one time, such as creating a document database.
- `README.md`: this file should contain instructions to understand how to use your code, and what SLURM environment is needed to run it.
If your scripts fail during the evaluation process we will usually look at this file to understand what is wrong and let you know.

## Question (Input) format.
Your program will be provided with a path to a CSV file using the same format as `questions.csv` format.

## Output format
The program will be provided with a path to directory in which it must write 3 files per question, one for the answer, one for the prompt used and one for the sources (list of url corresponding to the documents used).
Each of the files must be placed in the corresponding `answers`, `prompts` or `sources` directory.
The name of each files must be `<id>.txt` where `<id>` correspond
to the question id according to the CSV file. 

Output example:
```
/path/to/output_dir
├── answers
│   ├── first_id.txt
│   └── second_id.txt
├── prompts
│   ├── first_id.txt
│   └── second_id.txt
└── sources
    ├── first_id.txt
    └── second_id.txt
```

:warning: Source files should only contain URLs, with one URL per row:
```
https://example.com
http://example.com
https://peren.gouv.fr
```

# How to submit
When your work is ready, copy submission into the common project folder: `$ALL_CCFRWORK`, and ping us on the `#submissions` discord channel
