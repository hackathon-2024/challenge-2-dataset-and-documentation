<!--
SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
SPDX-License-Identifier: MIT
-->
# Datasets
Two datasets are available:
- a documentation database ;
- Sample questions and answers.

## Documentation database
The documentation database stems from the first challenge. It consists of a collection of raw HTML files and processed markdown files of API documentation and transparency reports. It contains useful information to answer questions in the dataset.

A *resource* is either the content of a web page or a PDF file. The database contains +1000 *resources* that are clearly identified with their URL.

Two different databases are available
-  Raw HTML files here: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-snapshots 
-  Markdown files (structured version of the HTML page) here: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

Be aware that a markdown document may contain multiple resources.

You may use other sources of information as long as they are clearly listed and made available during your code evaluation (see submission format below).

## Questions & Answers
The dataset of questions and answers contains around 120 questions (some questions include similar answers but phrased differently).
The dataset is split into two parts: a train set (10 questions) and a test set (the remainder) that will have exactly the same structure.

There will be no question unrelated to online platforms, but there may be questions for which no answer can be found in the sources. 

### Questions
Questions are located in a CSV file **using `;` as separators and `"` as delimiters when needed**.
The CSV contains two columns, one named `id` and one named `question`.

Our CSV format example:
```csv
id;question
first_id;How to win the hackathon?
second_id;"What is ""PEReN""?"
```

Please note that the `id` is an arbitrary string and the question can be in any language of the European Union.
Examples of questions are located in the `dataset/train/input/questions.csv` folder.

### Answers and sources
Example answers are located in `dataset/train/output/answers/`. Each answer is a text file, when multiple answers are available for the same questions, they are gathered in a folder named with the question id.

Sources are also text files identified with the question id, located in the `output/sources` folder. 
Note that some answers rely on multiple sources in the database. In this case, there will be as many lines in the text file as there are sources for the question.
