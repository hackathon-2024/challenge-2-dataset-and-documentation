## Registration procedure
Once your account has been validated by IDRIS, you will receive a username. You can only connect to Jean Zay _via_ our IP.
That is why you need first to connect to our (PEReN) infrastructure and from there connect to Jean Zay.  
You need to send to us by email the *public* SSH keys of all of your team members that will be used to login to our
infrastructure.
 
## Accessing the Jean-Zay supercomputer

```
ssh -J hackathon@infra2.peren.fr:1502 <username>@jean-zay.idris.fr
```

The fingerprint of the public SSH host key for `infra2.peren.fr` is:
```
SHA256:vN9oapdjEPN5SP2EfX669EKx6Ab7GJekU/aYwlt1cRE
```

A fallback proxy host is `rag-race.peren.fr`. This proxy is not expected to be used unless the hackathon staff members told you so.

```
ssh -J hackathon@rag-race.peren.fr:2222 <username>@jean-zay.idris.fr
```

The fingerprint of the public SSH host key for `rag-race.peren.fr` is:
```
SHA256:HEVto6mFfggxivGSTqRtlmWTs6l8KfIahXuPmMvkXh8
```

The [IDRIS documentation](http://www.idris.fr/eng/jean-zay/pre-post/jean-zay-jupyterhub-eng.html)
explains how to access to [JupyterHub](https://jupyterhub.idris.fr/).

## How to use the Jean-Zay supercomputer ?
### Important considerations
Jean-Zay being a shared supercomputer, you cannot run program on Jean-Zay directly.
You have to use a reservation system which will allocate ressources CPU, GPU and memory ressources to you,
for a certain amount of time.
The more ressources you ask for, the more you will have to wait before your program can run.

After logging in to Jean-Zay, you first arrive to a front-end server.
From this server you will be able to set up your computing environment,
and launch computing jobs with the reservation system.
**The front-end does not have computing power but it has access to the internet.**
**The back-end, on which your jobs will run, has no access to the internet.**
That means your jobs need to be working in an offline environment, and you will
need to download data, dependencies and deep-learning models beforehand.

The front-end and the back-end share storage space, so you can set up your
environment in front-end and use it in the back-end.

Personal storage space is divided into three folder:
- `$HOME`: with very limited storage (~3Go)
- `$WORK`: which runs on very fast hard-drive disks.
- `$SCRATCH`: which runs on SSDs.

**WARNING: Since `$HOME` space is very limited you must put your data and work on either`$WORK` or `$SCRATCH`.**


### Submitting a job
Jean-Zay uses SLURM to run jobs.
Here are exemple command lines to submit jobs.

#### Reserving a single V100 GPU, with either 16 or 32 GB of VRAM, for 1 hour, on an interactive job:
```bash
srun -A <3-letter ID>@v100 --qos=qos_gpu-dev --gres=gpu:1 --cpus-per-task=10 --hint=nomultithread --time=01:00:00 --pty -- bash
```
Breaking-down the command line:
- `srun`: command to interact with the reservation system
- `-A <3-letter ID>@v100`: use time reservation for V100 GPUs (other options are `<3-letter ID>@a100` and `<3-letter ID>@cpu` for A100 and CPU respectively). Your 3-letter id will be sent by email but you can also see it if you run the command `idrproj`.
- `--qos=qos_gpu-dev`: development QoS, faster reservation time but limited to 2 hours.
Remove to reserve ressources for more than 2 hours
- `--gres=gpu:1`: reserve one GPU
- `--cpus-per-task=10 --hint=nomultithread`: reserve 10 CPUs and 10*4 Go of RAM.
More [information on relationship between cpu and RAM reservation here](http://www.idris.fr/jean-zay/gpu/jean-zay-gpu-exec_alloc-mem.html)
- `--time=01:00:00`: reserve computing ressources for one hour
- `--pty`: interactive job
- `-- bash`: command to run on the compute nodes

#### Reserving two V100 GPUs with 32GB of VRAM for 30 minutes:
```bash
srun --qos=qos_gpu-dev --pty --gres=gpu:2 --cpus-per-task=10 --hint=nomultithread -C v100-32g --time=00:30:00 -- bash
```
With `-C v100-32g` we select [the 32GB of VRAM partition](http://www.idris.fr/jean-zay/gpu/jean-zay-gpu-exec_partition_slurm.html)

### Using environment
A list [pre-configured and optimized computing environments](http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-doc_module.html) are already available on Jean-Zay.
To use them, use the command `module load <module_name>`.

For instance to use pytorch-gpu on python:
```bash
module load pytorch-gpu/py3/2.1.1
```
The list of available modules is available with the command `module avail`.


For python users it is possible to stack pre-configured environment with venv.
First load the pre-configured modules that you want, and then source your venv.
```bash
module load pytorch-gpu/py3/2.1.1
python -m venv .venv
.venv/bin/pip install --no-cache-dir -r requirements.txt
source .venv/bin/activate
```

**Warning:** when using `pip` it is important to disable package caching (`--no-cache-dir`),
otherwise you will fill-up your $WORK space very fast.

Once an environment is sourced, it will carry-on to compute nodes in the back-end
if you are using interactive mode.


### List all available models
A lot of deep-learning models are already downloaded on Jean-Zay.
They can be found in the `$DSDIR` directory.

For instance, you can access huggingface models with the following command:
```
cd $DSDIR/HuggingFace_Models
```

### Common pitfalls
- **Out of space when downloading data or installing packets:** don't forget to work on `$WORK` or `$SCRATCH` directory.
If you are using package managers, check the caching options and use one of the following options: disable caching, change caching path to `$WORK` or `$SCRATCH` or make a symbolic link.
- **Program on the back-end freeze:** A lot of deep learning libraries download models from the internet. Make sure this is not the case, as your program will wait for HTTP request timeout, which can take a minute per request.
- **Program running on the back-end has been killed:** Make sure you use a sufficient timer option when running your job. It defaults to only 10 minutes.


### Useful Documentation
- [Starting guide](http://www.idris.fr/media/ia/guide_nouvel_utilisateur_ia-eng.pdf) ([fr](http://www.idris.fr/media/ia/guide_nouvel_utilisateur_ia.pdf))
- [Using GPUs](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-exec_partition_slurm-eng.html)
- [Memory allocation when using GPUs](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-exec_alloc-mem-eng.html)
- [Set up python environment](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-python-env-eng.html)
- [IDRIS Cheat sheet: Guide to Linux, module, SLURM & IDRIS commands](http://www.idris.fr/media/su/idrischeatsheet.pdf)
- [Access to JupyterHub](http://www.idris.fr/eng/jean-zay/pre-post/jean-zay-jupyterhub-eng.html)
### Basic commands:
- See your remaining hours: `idractt` (`idractt -a V100` if you want to see hours specifically allocated to V100 GPUs)
